-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-02-2021 a las 22:40:24
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inchikdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `description` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `logs`
--

INSERT INTO `logs` (`id`, `date`, `description`, `priority`) VALUES
(106, '2021-02-28', 'Modificar Stock [{\"id\":52,\"materialName\":\"asdasdasd\",\"productName\":null,\"quantity\":\"-1\"}] se completo correctamente. el Sun Feb 28 2021 17:28:16 GMT-0300 (Argentina Standard Time)', NULL),
(107, '2021-02-28', 'Modificar Stock [{\"id\":52,\"materialName\":\"asdasdasd\",\"productName\":null,\"quantity\":\"0\"}] se completo correctamente. el Sun Feb 28 2021 17:28:28 GMT-0300 (Argentina Standard Time)', NULL),
(108, '2021-02-28', 'Modificar Stock [{\"id\":52,\"materialName\":\"asdasdasd\",\"productName\":null,\"quantity\":\"1\"}] se completo correctamente. el Sun Feb 28 2021 17:28:37 GMT-0300 (Argentina Standard Time)', NULL),
(109, '2021-02-28', 'Guardar Material asdasd1w3123fsdf se completo correctamente. el Sun Feb 28 2021 17:28:53 GMT-0300 (Argentina Standard Time)', NULL),
(110, '2021-02-28', 'Modificar Stock [{\"id\":52,\"materialName\":\"asdasdasd\",\"productName\":null,\"quantity\":\"-1\"},{\"id\":53,\"materialName\":\"asdasd1w3123fsdf\",\"productName\":null,\"quantity\":0}] se completo correctamente. el Sun Feb 28 2021 17:29:06 GMT-0300 (Argentina Standard Time)', NULL),
(111, '2021-02-28', 'Modificar Stock [{\"id\":53,\"materialName\":\"asdasd1w3123fsdf\",\"productName\":null,\"quantity\":\"1\"}] se completo correctamente. el Sun Feb 28 2021 17:30:13 GMT-0300 (Argentina Standard Time)', NULL),
(112, '2021-02-28', 'Modificar Stock [{\"id\":52,\"materialName\":\"asdasdasd\",\"productName\":null,\"quantity\":\"1\"}] se completo correctamente. el Sun Feb 28 2021 17:32:04 GMT-0300 (Argentina Standard Time)', NULL),
(113, '2021-02-28', 'Modificar Stock [{\"id\":52,\"materialName\":\"asdasdasd\",\"productName\":null,\"quantity\":\"-1\"},{\"id\":53,\"materialName\":\"asdasd1w3123fsdf\",\"productName\":null,\"quantity\":\"0\"}] se completo correctamente. el Sun Feb 28 2021 17:38:42 GMT-0300 (Argentina Standard Time)', NULL),
(114, '2021-02-28', 'Modificar Stock [{\"id\":52,\"materialName\":\"asdasdasd\",\"productName\":null,\"quantity\":\"3\"},{\"id\":53,\"materialName\":\"asdasd1w3123fsdf\",\"productName\":null,\"quantity\":\"3\"}] se completo correctamente. el Sun Feb 28 2021 17:40:45 GMT-0300 (Argentina Standard Time)', NULL),
(115, '2021-02-28', 'Modificar Stock [{\"id\":52,\"materialName\":\"asdasdasd\",\"productName\":null,\"quantity\":\"0\"},{\"id\":53,\"materialName\":\"asdasd1w3123fsdf\",\"productName\":null,\"quantity\":3}] se completo correctamente. el Sun Feb 28 2021 17:41:58 GMT-0300 (Argentina Standard Time)', NULL),
(116, '2021-02-28', 'Modificar Stock [{\"id\":52,\"materialName\":\"asdasdasd\",\"productName\":null,\"quantity\":\"1\"},{\"id\":53,\"materialName\":\"asdasd1w3123fsdf\",\"productName\":null,\"quantity\":3}] se completo correctamente. el Sun Feb 28 2021 18:03:58 GMT-0300 (Argentina Standard Time)', NULL),
(117, '2021-02-28', 'Modificar Stock [{\"id\":53,\"materialName\":\"asdasd1w3123fsdf\",\"productName\":null,\"quantity\":\"-2\"},{\"id\":52,\"materialName\":\"asdasdasd\",\"productName\":null,\"quantity\":\"0\"}] se completo correctamente. el Sun Feb 28 2021 18:14:06 GMT-0300 (Argentina Standard Time)', NULL),
(118, '2021-02-28', 'Modificar Stock [{\"id\":52,\"materialName\":\"asdasdasd\",\"productName\":null,\"quantity\":\"1\"},{\"id\":53,\"materialName\":\"asdasd1w3123fsdf\",\"productName\":null,\"quantity\":\"1\"}] se completo correctamente. el Sun Feb 28 2021 18:36:44 GMT-0300 (Argentina Standard Time)', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materials`
--

CREATE TABLE `materials` (
  `id` int(11) NOT NULL,
  `cost` float NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `price` float NOT NULL,
  `recipe` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `cart` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`cart`)),
  `cost` float NOT NULL,
  `profit` float NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `userInfo` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`userInfo`)),
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `materialName` varchar(500) DEFAULT NULL,
  `productName` varchar(500) DEFAULT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `productName` (`productName`),
  ADD UNIQUE KEY `materialName` (`materialName`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT de la tabla `materials`
--
ALTER TABLE `materials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT de la tabla `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
