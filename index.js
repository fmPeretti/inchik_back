const express = require("express")
const app = express()
const bodyParser = require("body-parser")
const cors = require("cors")
const mysql = require("mysql")
const db = mysql.createPool({
    host: "localhost",
    user: "root",
    password: "",
    database: "inchikdb",
});
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());
app.use(express.json())

//LOG MANAGER//
let logManager = (action, actionResult, error) =>{
    const date = new Date();
    const msg = action + (
                    actionResult ?
                    " se completo correctamente. el "+ date.toString()
                    :
                    " ha fallado. el " + date.toString() + ". Por el error: " + error
                );
    const priority = actionResult ? null : "FAILED";
    const logQuery = "INSERT INTO logs (description, date, priority) VALUES (?, ?, ?)";
    db.query(logQuery,[msg, date, priority], (response,err)=>{

    });
}
///START PRODUCTS///
app.get("/retrieveProductById", (req, res)=>{
    const sqlSelect = "SELECT * FROM products WHERE id=?";
    db.query(sqlSelect, [req.query.id], (err, result)=>{
        res.send(result);
    });
});
app.get("/search/products", (req, res)=>{
    const sqlInsert = "SELECT * FROM products";
    db.query(sqlInsert, (err, result)=>{
        res.send(result);
    });
});
app.post("/save/product", (req, res) =>{  
    const name = req.body.name;
    const description = req.body.description;
    const recipe = JSON.stringify(req.body.recipe);
    const price = req.body.price;
    const firstStock = req.body.firstStock;
    const saveQuery = "INSERT INTO products (name, price, recipe, description) VALUES (?, ?, ?, ?)";
    const stockQuery = "INSERT INTO stocks (productName, quantity) VALUES (?, ?)";
    db.query(saveQuery, [name, price, recipe, description], (err, result)=>{
        if(err!=null){
            logManager("Guardado de Producto " + name , false, err.sqlMessage);
            err.statusText=err.sqlMessage;
            err.code="500";
            res.send(err);
        }else{
            //Stock Managment
            db.query(stockQuery, [name ,firstStock], (errS, resultS)=>{
                if(errS!=null){
                    logManager("Guardado de Producto "+ name, false, errS.sqlMessage);
                    err.statusText=errS.sqlMessage;
                    err.code="500";
                    res.send(err);
                }else{
                    logManager("Guardado de Producto "+ name, true);
                    res.send(result);
                }
            });
        }
    });

})
app.post("/update/product", (req, res)=>{
    const id = req.body.id;
    const name = req.body.name;
    const description = req.body.description;
    const price = req.body.price;
    const recipe = JSON.stringify(req.body.recipe);
    const updateQuery = "UPDATE products SET name=?, price=?, recipe=?, description=? WHERE id=?";
    db.query(updateQuery, [name, price, recipe, description, id], (err, result)=>{
        if(err!=null){
            logManager("Actualización de Producto "+ name, false, err.sqlMessage);
            err.statusText=err.sqlMessage;
            err.code="500";
            res.send(err);
        }else{
            logManager("Actualización de Producto "+ name, true);
            res.send(result);
        }
    });
});
app.post("/delete/products", (req, res)=>{
    const id = req.body.id;
    const name = req.body.name
    const deleteQuery = "DELETE FROM products WHERE id=?"
    const stockQuery = "DELETE FROM stocks WHERE productName=?"
    db.query(deleteQuery, [id], (err, response)=>{
        if(err!=null){
            logManager("Eliminar Producto "+ name, false, err.sqlMessage);
            err.statusText=err.sqlMessage;
            err.code="500";
            res.send(err);
        }else{
            //Stock Managment
            db.query(stockQuery, [name], (errS, resultS)=>{
                if(errS!=null){
                    logManager("Eliminar Producto "+ name, false, errS.sqlMessage);
                    err.statusText=errS.sqlMessage;
                    err.code="500";
                    res.send(err);
                }else{
                    logManager("Eliminar de Producto "+ name, true);
                    res.send(response);
                }
            });
        }
    })
})

///END PRODUCTS///

///START MATERIALS///
app.get("/retrieveMaterialById", (req, res)=>{
    const sqlSelect = "SELECT * FROM materials WHERE id=?";
    db.query(sqlSelect, [req.query.id], (err, result)=>{
        res.send(result);

    });
});
app.get("/search/materials", (req, res)=>{
    const sqlSelect = "SELECT * FROM materials";
    db.query(sqlSelect, (err, result)=>{
        res.send(result);

    });
});
app.post("/save/materials", (req, res) =>{  
    const name = req.body.name;
    const description = req.body.description;
    const cost = req.body.cost;
    const quantity = req.body.firstStock;
    const saveQuery = "INSERT INTO materials (name, cost, description) VALUES (?, ?, ?)";
    const stockQuery = "INSERT INTO stocks (materialName, quantity) VALUES (?, ?)";
    db.query(saveQuery, [name, cost, description], (err, result)=>{
        if(err!=null){
            logManager("Guardar Material "+ name, false, err.sqlMessage);
            err.statusText=err.sqlMessage;
            err.code="500";
            res.send(err);
        }else{
            //Stock Managment
            db.query(stockQuery, [name ,quantity], (errS, resultS)=>{
                if(errS!=null){
                    logManager("Guardar Material "+ name, false, errS.sqlMessage);
                    err.statusText=errS.sqlMessage;
                    err.code="500";
                    res.send(err);
                }else{
                    logManager("Guardar Material " + name, true);
                    res.send(result);
                }
            });
        }
    });

})
app.post("/update/material", (req, res)=>{
    const id = req.body.id;
    const name = req.body.name;
    const description = req.body.description;
    const cost = req.body.cost;
    const updateQuery = "UPDATE materials SET name=?, cost=?,description=? WHERE id=?";
    db.query(updateQuery, [name, cost, description, id], (err, result)=>{
        if(err!=null){
            logManager("Actualizar Material "+ name, false, err.sqlMessage);
            err.statusText=err.sqlMessage;
            err.code="500";
            res.send(err);
        }else{
            logManager("Actualizar Material "+ name, true);
            res.send(result);
        }
    });
});
app.post("/delete/materials", (req, res)=>{
    const id = req.body.id;
    const name = req.body.name
    const deleteQuery = "DELETE FROM materials WHERE id=?"
    const stockQuery = "DELETE FROM stocks WHERE materialName=?"
    db.query(deleteQuery, [id], (err, response)=>{
        if(err!=null){
            logManager("Eliminar Material "+ name, false, err.sqlMessage);
            err.statusText=err.sqlMessage;
            err.code="500";
            res.send(err);
        }else{
            //Stock Managment
            db.query(stockQuery, [name], (errS, resultS)=>{
                if(errS!=null){
                    logManager("Eliminar Material "+ name, false, errS.sqlMessage);
                    err.statusText=errS.sqlMessage;
                    err.code="500";
                    res.send(err);
                }else{
                    logManager("Eliminar Material "+ name, true);
                    res.send(response);
                }
            });
        }
    })
})
///END MATERIALS///

///START STOCKS///
app.get("/retrieveStockById", (req, res)=>{
    const sqlSelect = "SELECT * FROM stocks WHERE id=?";
    db.query(sqlSelect, [req.query.id], (err, result)=>{
        res.send(result);
    });
});
app.get("/search/stocks", (req, res)=>{
    const sqlSelect = "SELECT * FROM stocks";
    db.query(sqlSelect, (err, result)=>{
        res.send(result);
    });
});

app.post("/update/stocks", (req, res)=>{
    let errCheck = 0;
    let errSave = "";
    let list = req.body;
    let stockUpdate = new Promise((resolve, reject)=>{
        let count=0;
        list.forEach(item=>{
            const type = item.productName==null;
            const quantity = item.quantity;
            const name = type ? item.materialName : item.productName;
            const updateQuery = type ? "UPDATE stocks SET quantity=? WHERE materialName=?" : "UPDATE stocks SET quantity=? WHERE productName=?";
            db.query(updateQuery, [ quantity, name ], (err, response)=>{
                count++
                if(err!=null){
                    err.statusText=err.sqlMessage;
                    err.code="500";
                    errSave = err;
                }else{
                    errCheck++;
                }
                if(errCheck==list.length && count==list.length){
                    resolve();
                }else if (errCheck!=list.length && count==list.length){
                    reject(errSave);
                }
            });
        });
    });
    stockUpdate.then(()=>{
        logManager("Modificar Stock " + JSON.stringify(list), true);
        res.send("Todos los Stocks se modificaron con éxito");
    }).catch((e)=>{
        logManager("Modificar Stocks " + JSON.stringify(list), false, e);
        res.send(e);
    })

});

///END STOCKS///

///START SALES///
app.get("/retrieveSaleById", (req, res)=>{
    const sqlSelect = "SELECT * FROM sales WHERE id=?";
    db.query(sqlSelect, [req.query.id], (err, result)=>{
        res.send(result);
    });
});
app.get("/search/sales", (req, res)=>{
    const sqlSelect = "SELECT * FROM sales";
    db.query(sqlSelect, (err, result)=>{
        res.send(result);
    });
});
app.post("/save/sales", (req, res) =>{  
    const description = req.body.description;
    const cost = req.body.cost;
    const profit = req.body.profit;
    const cart = req.body.cart;
    const cartToSave = JSON.stringify(req.body.cart);
    const date = req.body.date;
    const userInfo = JSON.stringify(req.body.userInfo);
    const saveQuery = "INSERT INTO sales (date, cart, userInfo, description, cost, profit) VALUES (?, ?, ?, ?, ?, ?)";
    let errSave;
    let cartCheck = new Promise((resolve, reject)=>{
        let errCheck=0;
        let count=0;
        cart.forEach((item)=>{
            let stockQuery = "UPDATE stocks SET quantity = quantity - ? WHERE productName = ?";
            db.query(stockQuery,[item.quantity, item.name], (errS, resultS)=>{
                count++;
                if(errS!=null){
                    errS.statusText=errS.sqlMessage;
                    errS.code="500";
                    errSave = errS;
                }else{
                    errCheck++;
                }
                if(errCheck==cart.length && count==cart.length){
                    resolve();
                }else if (errCheck!=cart.length && count==cart.length){
                    reject(errSave);
                }
            });
        })
    });

    db.query(saveQuery, [date, cartToSave, userInfo, description, cost, profit], (err, result)=>{
        if(err!=null){
            logManager("Guardar Venta " + cartToSave, false, err.sqlMessage);
            err.statusText=err.sqlMessage;
            err.code="500";
            res.send(err);
        }else{
           cartCheck.then(()=>{
                logManager("Guardar Venta " + cartToSave, true);
                res.send("La Venta se Registró Satisfactoriamente");
           }).catch( e =>{
                logManager("Guardar Venta " + cartToSave, false, errSave);
                res.send(errSave);
           })
        }
    });

})

app.post("/delete/sales", (req, res)=>{
    const id = req.body.id;
    const deleteQuery = "DELETE FROM sales WHERE id=?"
    db.query(deleteQuery, [id], (err, response)=>{
        if(err!=null){
            logManager("Eliminar Venta " + id , false, err.sqlMessage);
            err.statusText=err.sqlMessage;
            err.code="500";
            res.send(err);
        }else{
            logManager("Eliminar Venta ", true);
            res.send(response);
        }
    })
})
///END SALES///
app.listen(3001, ()=>{
});
